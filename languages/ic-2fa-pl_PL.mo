��          T      �       �   R   �        
   #     .     4  k   =  �  �  Y   ;     �     �     �     �  v   �                                        A text message with a verification code was just sent to ••• ••• •%s Didn't receive a code? Login code Phone SMS Code The login code you entered doesn't match the one sent to your phone. Please check the number and try again. Project-Id-Version: IC Two Factor Authentication
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
POT-Creation-Date: 
PO-Revision-Date: 
Language-Team: 
X-Generator: Poedit 1.8.10
Last-Translator: 
Language: pl_PL
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 SMS z kodem weryfikacyjnym został właśnie wysłany pod numer ••• ••• •%s Nie masz kodu? Kod logowania Telefon Kod SMS Wprowadzony kod logowania różni się od kodu wysłanego na Twój numer telefonu. Sprawdź numer i spróbuj ponownie. 