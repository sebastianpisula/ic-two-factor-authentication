<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/** @var \IC\TFA\Classes\View $this */
/** @var \IC\TFA\Modules\Settings $settings */
?>
<div class="wrap">
	<div id="icon-tools" class="icon32"></div>

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<?php $settings->admin_notices(); ?>

	<form method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>">
		<input type="hidden" name="action" value="tfa-settings-save"/>
		<?php wp_nonce_field( 'tfa-settings' ); ?>
		<fieldset>
			<p>
				<label>
					<strong><?php _e( 'Access Token', 'ic-2fa' ); ?></strong>
					<br/>
					<input required type="text" name="tfa_access_token"
						   value="<?php echo esc_attr( get_option( 'tfa_access_token' ) ); ?>"/>
				</label>
			</p>

			<?php submit_button( __( 'Save Changes', 'ic-2fa' ) ); ?>
		</fieldset>
	</form>
</div>