<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<style type="text/css">
	#loginform p:not(.submit) {
		display: none;
	}
</style>
<p class="user_code" style="display: block">
	<label for="user_code">
		<?php _e( 'SMS Code', 'ic-2fa' ); ?>
		<br/>
		<input placeholder="<?php _e( 'Login code', 'ic-2fa' ); ?>" autofocus type="text" name="code" id="user_code"
			   class="input js--code-field"
			   value="" size="20"/></label>

	<a href="<?php echo add_query_arg( [ 'action' => 'sendsms' ] ); ?>">
		<?php _e( 'Didn\'t receive a code?', 'ic-2fa' ); ?>
	</a>
</p>