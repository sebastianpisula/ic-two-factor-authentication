<?php

namespace IC\TFA\Modules;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use IC\TFA\Classes\Module;
use IC\TFA\Classes\Notify;

class Auth_Code extends Module {

	/**
	 * Init module
	 */
	protected function init() {
		add_filter( 'user_contactmethods', [ $this, 'user_contactmethods' ], 10, 2 );

		add_filter( 'authenticate', [ $this, 'authenticate_by_cookie_hash' ], 5, 3 );
		add_filter( 'authenticate', [ $this, 'authenticate_check_code_sms' ], 50, 3 );

		add_action( 'login_form_sendsms', [ $this, 'login_form_sendsms' ] );

		add_action( 'login_form', [ $this, 'login_form' ] );
		add_action( 'wp_login', [ $this, 'delete_data' ], 10, 2 );
	}

	/**
	 * Add new field in login form
	 */
	public function login_form() {
		if ( $this->get_user_by_cookie_hash() ) {
			$this->plugin->view->display( 'code-field.php' );
		}
	}

	/**
	 * Delete auth temporary data
	 *
	 * @param string   $user_login Username.
	 * @param \WP_User $user       WP_User object of the logged-in user.
	 */
	function delete_data( $user_login, $user ) {
		delete_user_meta( $user->ID, 'auth_code' );
		delete_user_meta( $user->ID, 'auth_code_expire' );
		delete_user_meta( $user->ID, 'auth_hash' );

		setcookie( 'auth_hash', '1', current_time( 'timestamp' ) - DAY_IN_SECONDS, null, COOKIE_DOMAIN );
	}

	/**
	 * Auth user via cookie hash
	 *
	 * @param null|\WP_User|\WP_Error $user     WP_User if the user is authenticated.
	 *                                          WP_Error or null otherwise.
	 * @param string                  $username Username or email address.
	 * @param string                  $password User password
	 *
	 * @return null|\WP_Error|\WP_User
	 */
	function authenticate_by_cookie_hash( $user, $username, $password ) {
		return $this->get_user_by_cookie_hash();
	}

	/**
	 * Auth user via sms code
	 *
	 * @param null|\WP_User|\WP_Error $user     WP_User if the user is authenticated.
	 *                                          WP_Error or null otherwise.
	 * @param string                  $username Username or email address.
	 * @param string                  $password User password
	 *
	 * @return null|\WP_Error|\WP_User
	 */
	function authenticate_check_code_sms( $user, $username, $password ) {
		if ( ! ( $user instanceof \WP_User ) ) {
			return $user;
		}

		$phone = (int) get_user_meta( $user->ID, 'phone', true );

		if ( $phone ) {
			$user_code = (int) get_user_meta( $user->ID, 'auth_code', true );

			if ( ! isset( $_COOKIE['auth_hash'] ) ) {

				$this->generate_token( $user->ID );

				$code_status = $this->send_notify( $user->ID );

				if ( is_wp_error( $code_status ) ) {
					$this->delete_data( '', $user );
				}

				if ( $code_status === true ) {
					wp_redirect( wp_login_url() );
					die();
				}

				return $code_status;
			} else if ( ! isset( $_POST['code'] ) ) {

				$message = sprintf( __( 'A text message with a verification code was just sent to ••• ••• •%s', 'ic-2fa' ), substr( $phone, - 2 ) );

				return new \WP_Error( 'no_code', $message, 'message' );
			} elseif ( intval( $_POST['code'] ) !== $user_code ) {
				return new \WP_Error( 'no_active', __( 'The login code you entered doesn\'t match the one sent to your phone. Please check the number and try again.', 'ic-2fa' ) );
			}
		}

		return $user;
	}

	/**
	 * Add field for phone.
	 *
	 * @param array    $methods Array of contact methods and their labels.
	 * @param \WP_User $user    WP_User object.
	 *
	 * @return array
	 */
	public function user_contactmethods( $methods, $user ) {
		$methods['phone'] = __( 'Phone', 'ic-2fa' );

		return $methods;
	}

	/**
	 * Send again code
	 */
	public function login_form_sendsms() {
		if ( $user = $this->get_user_by_cookie_hash() ) {
			$this->generate_token( $user->ID );
			$this->send_notify( $user->ID );
		}
	}

	/**
	 * Get user by cookie hash
	 *
	 * @return null|\WP_User
	 */
	private function get_user_by_cookie_hash() {
		if ( isset( $_COOKIE['auth_hash'] ) && ! empty( $_COOKIE['auth_hash'] ) ) {
			$user_query = new \WP_User_Query( [ 'meta_key' => 'auth_hash', 'meta_value' => $_COOKIE['auth_hash'] ] );

			if ( $results = $user_query->get_results() ) {
				return $results[0];
			}
		}

		return null;
	}

	/**
	 * Generate auth data
	 *
	 * @param int $user_id
	 */
	private function generate_token( $user_id ) {
		$hash = wp_hash( serialize( get_userdata( $user_id ) ) . wp_create_nonce( 'auth_hash' ), 'nonce' );

		update_user_meta( $user_id, 'auth_hash', $hash );
		update_user_meta( $user_id, 'auth_code', wp_rand( 1000, 9999 ) );
		update_user_meta( $user_id, 'auth_code_expire', current_time( 'timestamp' ) + HOUR_IN_SECONDS );

		setcookie( 'auth_hash', $hash, current_time( 'timestamp' ) + HOUR_IN_SECONDS, null, COOKIE_DOMAIN );
	}

	/**
	 * Send notify
	 *
	 * @param int $user_id
	 *
	 * @return mixed|\WP_Error
	 */
	private function send_notify( $user_id ) {
		$class_name = apply_filters( 'tfa/notify', 'IC\TFA\Classes\Notify\SMS' );

		if ( ! class_exists( $class_name ) ) {
			error_log( 'Class ' . $class_name . ' not exist. File: ' . __FILE__ . ':' . __LINE__ );

			return new \WP_Error( 'no-class', __( 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie później.', 'ic-2fa' ) );
		} else {
			$notify = new $class_name( $user_id );

			if ( ! $notify instanceof Notify ) {
				error_log( 'Class ' . $class_name . ' not exist. File: ' . __FILE__ . ':' . __LINE__ );

				return new \WP_Error( 'no-class', __( 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie później.', 'ic-2fa' ) );
			}
		}

		return $notify->send();
	}
}