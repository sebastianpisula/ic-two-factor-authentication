<?php

namespace IC\TFA\Modules;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use IC\TFA\Classes\Module;
use SMSApi\Api\UserFactory;
use SMSApi\Client;

class Settings extends Module {
	protected function init() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );

		add_action( 'admin_post_tfa-settings-save', [ $this, 'save_settings' ] );
	}

	public function admin_menu() {
		add_users_page(
			__( 'Two Factor Authentication Settings', 'ic-2fa' ),
			__( 'Two Factor Authentication', 'ic-2fa' ),
			'manage_options',
			'ic-tfa-settings',
			[ $this, 'settings_page' ]
		);
	}

	public function settings_page() {
		$this->plugin->view->set_vars( [ 'settings' => $this ] );
		$this->plugin->view->display( 'admin/settings.php' );
	}

	public function save_settings() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'Sorry, you are not allowed to do that.' ) );
		}

		check_admin_referer( 'tfa-settings' );

		$type = 'success';
		$info = __( 'Ustawienia zostały zapisane', 'ic-2fa' );

		if ( isset( $_POST['tfa_access_token'] ) ) {
			if ( $this->valid_token( $_POST['tfa_access_token'] ) ) {
				update_option( 'tfa_access_token', esc_sql( $_POST['tfa_access_token'] ) );
			} else {
				$type = 'error';
				$info = __( 'Podany token jest nieprawidłowy lub brak uprawnień do wysyłania SMS.', 'ic-2fa' );
			}
		}

		$url = add_query_arg(
			array(
				'type' => $type,
				'info' => urlencode( $info )
			), wp_get_referer() );

		wp_redirect( $url );
		die();
	}

	/**
	 * Check valid token
	 *
	 * @param string $token
	 *
	 * @return bool
	 */
	private function valid_token( $token ) {
		try {
			$u = new UserFactory();
			$u->setClient( Client::createFromToken( $token ) );

			$u->actionGetPoints()->execute();

			return true;
		} catch ( \Exception $exception ) {
			return false;
		}
	}

	public function admin_notices() {
		if ( isset( $_GET['info'] ) && isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'success', 'error' ] ) ) {
			echo '<div class="notice notice-' . esc_attr( $_GET['type'] ) . '"><p>' . esc_html( $_GET['info'] ) . '</p></div>';
		}
	}
}