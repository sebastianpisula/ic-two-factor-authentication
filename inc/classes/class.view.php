<?php

namespace IC\TFA\Classes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class View {
	/** @var array */
	private $dirs = [];

	/** @var array */
	private $vars = [];

	/**
	 * Register dir for template
	 *
	 * @param string $dir
	 *
	 * @return bool
	 */
	public function register_dir( $dir ) {
		$check = file_exists( $dir );

		if ( $check ) {
			$this->dirs[] = $dir;
		}

		return $check;
	}

	/**
	 * Set vars
	 *
	 * @param $vars
	 */
	public function set_vars( $vars ) {
		$this->vars = $vars;
	}


	/**
	 * Show view
	 *
	 * @param string $file
	 */
	public function display( $file ) {
		echo $this->get( $file );
	}

	/**
	 * Retrieve view
	 *
	 * @param string $file
	 *
	 * @return string
	 */
	public function get( $file ) {

		extract( $this->vars );

		ob_start();

		$path = $this->get_template_file( $file );

		if ( $path ) {
			include $path;
		} else {
			return sprintf( __( 'Template %s not exists' ), $file );
		}

		$html = ob_get_clean();

		return $html;
	}

	/**
	 * Retrieve template file path if exists or false.
	 *
	 * @param string $file
	 *
	 * @return bool|string
	 */
	private function get_template_file( $file ) {
		foreach ( $this->dirs AS $dir ) {
			$path = wp_normalize_path( $dir . '/' . $file );

			if ( file_exists( $path ) ) {
				return $path;
			}
		}

		return false;
	}
}