<?php

namespace IC\TFA\Classes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

abstract class Notify implements \IC\TFA\Interfaces\Notify {
	protected $user_id;

	public function __construct( $user_id ) {
		$this->user_id = $user_id;
	}
}