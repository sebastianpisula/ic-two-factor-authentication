<?php

namespace IC\TFA\Interfaces;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

interface Notify {
	public function send();
}