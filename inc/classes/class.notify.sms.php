<?php

namespace IC\TFA\Classes\Notify;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use IC\TFA\Classes\Notify;
use SMSApi\Api\SmsFactory;
use SMSApi\Client;
use SMSApi\Exception\SmsapiException;

class SMS extends Notify {

	/**
	 * Get user number
	 *
	 * @return string
	 */
	private function get_to() {
		return get_user_meta( $this->user_id, 'phone', 1 );
	}

	/**
	 * Get text message
	 *
	 * @return string
	 */
	private function get_message() {
		$code = get_user_meta( $this->user_id, 'auth_code', 1 );

		return sprintf( __( 'Twój kod logowania: %s', 'ic-2fa' ), $code );
	}

	/**
	 * Get access token
	 *
	 * @return string
	 */
	private function get_token() {
		return get_option( 'tfa_access_token', '' );
	}

	/**
	 * Send message
	 *
	 * @return bool|\WP_Error
	 */
	public function send() {
		$client = Client::createFromToken( $this->get_token() );

		$smsapi = new SmsFactory();
		$smsapi->setClient( $client );

		try {
			$actionSend = $smsapi->actionSend();

			$actionSend->setTo( $this->get_to() );
			$actionSend->setText( $this->get_message() );
			$actionSend->setSender( 'Info' );

			$actionSend->execute();

			return true;
		} catch ( SmsapiException $exception ) {
			error_log( 'SMSAPI Error: ' . trim( $exception->getMessage() ) );

			return new \WP_Error( 'api-error', __( 'Wystąpił błąd podczas wysyłania SMS z kodem. Proszę spróbować za chwilę.', 'ic-2fa' ) );
		}
	}
}