<?php

namespace IC\TFA\Classes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

abstract class Module {
	/** @var \IC_TFA_Plugin */
	protected $plugin;

	/**
	 * Module constructor.
	 *
	 * @param \IC_TFA_Plugin $plugin
	 */
	public function __construct( \IC_TFA_Plugin $plugin ) {
		$this->plugin = $plugin;

		$this->init();
	}

	/**
	 * Init module
	 */
	protected function init() {
	}
}