var gulp = require('gulp');
var pot = require('gulp-wp-pot');

gulp.task('pot', function () {
    return gulp.src('./**/*.php')
        .pipe(pot({
            domain: 'ic-2fa',
            package: 'IC Two Factor Authentication'
        }))
        .pipe(gulp.dest('./languages/ic-2fa.pot'));
});