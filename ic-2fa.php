<?php

/**
 * Plugin Name: IC Two Factor Authentication
 * Author: Sebastian Pisula
 * Author URI: mailto:sebastian.pisula@gmail.com
 * Description: Secure WordPress login with Two Factor Authentication
 * Text Domain: ic-2fa
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include 'vendor/autoload.php';

class IC_TFA_Plugin {
	/** @var IC\TFA\Classes\View */
	public $view;

	/** @var string */
	private $dir;

	/** @var string */
	private $slug;

	/** @var string */
	private $file;

	/** @var string */
	private $basename;

	/**
	 * Plugin constructor.
	 *
	 * @param string $file
	 */
	public function __construct( $file ) {
		$this->file     = $file;
		$this->dir      = dirname( $file );
		$this->slug     = wp_basename( $this->dir );
		$this->basename = plugin_basename( $file );

		add_action( 'plugins_loaded', [ $this, 'plugins_loaded' ] );
		add_filter( 'plugin_action_links', [ $this, 'plugin_action_links' ], 10, 2 );

		register_activation_hook( __FILE__, [ $this, 'activation' ] );
	}

	/**
	 * Hook after activation plugin
	 */
	function activation() {
		if ( version_compare( PHP_VERSION, '5.6', '<' ) ) {
			deactivate_plugins( $this->basename );
			wp_die( __( 'This plugin requires PHP in version at least 5.6.', 'ic-2fa' ) );
		}
	}

	/**
	 * Add url to settings page
	 *
	 * @param array  $actions
	 * @param string $plugin_file
	 *
	 * @return array
	 */
	function plugin_action_links( $actions, $plugin_file ) {
		if ( $plugin_file == $this->basename ) {
			$actions[] = '<a href="' . menu_page_url( 'ic-tfa-settings', false ) . '">' . __( 'Settings', 'ic-2fa' ) . '</a>';
		}

		return $actions;
	}

	/**
	 * Register modules
	 */
	public function plugins_loaded() {
		__( 'IC Two Factor Authentication', 'ic-2fa' );
		__( 'Secure WordPress login with Two Factor Authentication', 'ic-2fa' );

		$this->view = new IC\TFA\Classes\View();
		$this->view->register_dir( $this->dir . '/inc/views/' );

		load_plugin_textdomain( $this->slug, false, $this->slug . '/languages' );

		new IC\TFA\Modules\Auth_Code( $this );
		new IC\TFA\Modules\Settings( $this );
	}
}

new IC_TFA_Plugin( __FILE__ );